import { Document, Schema, Model, model} from 'mongoose';
import { IExample } from '../../shared/interfaces';

interface IExampleModel extends IExample, Document {

}

const ExampleSchema: Schema = new Schema({
  name: String,
  age: String
});

export const Example: Model<IExampleModel> = model<IExampleModel>('Example', ExampleSchema);

