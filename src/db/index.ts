import mongoos from 'mongoose';
import config from 'config';
import * as declaredModels from './models';
import {logger} from '../logger';

export function connectToDb() {
    mongoos.connect(config.get('dbURI'));
}


mongoos.connection.on('error', () => logger.error('error connecting to db', {actionId: 'db'}));
mongoos.connection.once('open', () => logger.info('successfuly connected to db', {actionId: 'db'}));

export const models = declaredModels;
