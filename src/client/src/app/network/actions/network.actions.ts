import { Action } from '@ngrx/store';
import { INetwork } from '../../../../../shared/interfaces/network.interface';
import { Update } from '@ngrx/entity';

export enum NetworkActionTypes {
  LoadNetworks = '[Network] Load Networks',
  CreateNetwork = '[Network] Create Network',
  UpdateNetwork = '[Network] Update Network',
  DeleteNetwork = '[Network] Delete Network',
  SelectNetwork = '[Network] Select Network',
  DeselectNetwork = '[Network] Deselect Network',
}

export class LoadNetworks implements Action {
  readonly type = NetworkActionTypes.LoadNetworks;
}

export class CreateNetwork implements Action {
  readonly type = NetworkActionTypes.CreateNetwork;
  constructor(public payload: INetwork) { }
}

export class UpdateNetwork implements Action {
  readonly type = NetworkActionTypes.UpdateNetwork;
  constructor(public payload: Update<INetwork>) { }
}

export class DeleteNetwork implements Action {
  readonly type = NetworkActionTypes.DeleteNetwork;
  constructor(public payload: string) { }
}

export class SelectNetwork implements Action {
  readonly type = NetworkActionTypes.SelectNetwork;
  constructor(public payload: string) { }
}

export class DeselectNetwork implements Action {
  readonly type = NetworkActionTypes.DeselectNetwork;
}

export type NetworkActions =
  LoadNetworks |
  CreateNetwork |
  UpdateNetwork |
  DeleteNetwork |
  SelectNetwork |
  DeselectNetwork;
