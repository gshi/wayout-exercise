import { Action } from '@ngrx/store';
import { INetwork } from '../../../../../shared/interfaces/network.interface';
import { Update } from '@ngrx/entity';

export enum NetworkApiActionTypes {
  LoadNetworksSuccess = '[Network Api] Load Networks success',
  LoadNetworksFailed = '[Network Api] Load Networks failed',
  CreateNetworkSuccess = '[Network Api] Create Network success',
  CreateNetworkFailed = '[Network Api] Create Network failed',
  UpdateNetworkSuccess = '[Network Api] Update Network success',
  UpdateNetworkFailed = '[Network Api] Update Network failed',
  DeleteNetworkSuccess = '[Network Api] Delete Network success',
  DeleteNetworkFiled = '[Network Api] Delete Network failed'
}

export class LoadNetworksSuccess implements Action {
    readonly type = NetworkApiActionTypes.LoadNetworksSuccess;
    constructor (public payload: INetwork[]) {}
}

export class LoadNetworksFailed implements Action {
    readonly type = NetworkApiActionTypes.LoadNetworksFailed;
}

export class CreateNetworkSuccess implements Action {
    readonly type = NetworkApiActionTypes.CreateNetworkSuccess;
    constructor (public payload: INetwork) {}
}

export class CreateNetworkFailed implements Action {
    readonly type = NetworkApiActionTypes.CreateNetworkFailed;
}

export class UpdateNetworkSuccess implements Action {
    readonly type = NetworkApiActionTypes.UpdateNetworkSuccess;
    constructor (public payload: Update<INetwork>) {}
}

export class UpdateNetworkFailed implements Action {
    readonly type = NetworkApiActionTypes.UpdateNetworkFailed;
}

export class DeleteNetworkSuccess implements Action {
    readonly type = NetworkApiActionTypes.DeleteNetworkSuccess;
    constructor (public payload: string) {}
}

export class DeleteNetworkFailed implements Action {
    readonly type = NetworkApiActionTypes.DeleteNetworkFiled;
}

export type NetworkApiActions =
    LoadNetworksSuccess|
    LoadNetworksFailed|
    CreateNetworkSuccess|
    CreateNetworkFailed|
    UpdateNetworkSuccess|
    UpdateNetworkFailed|
    DeleteNetworkSuccess|
    DeleteNetworkFailed;
