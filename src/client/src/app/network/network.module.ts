import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NetworkRoutingModule } from './network-routing.module';
import { NetworksPageComponent } from './containers/networks-page/networks-page.component';
import { SharedModule } from '../shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromNetwork from './reducers/network.reducer';
import { EffectsModule } from '@ngrx/effects';
import { NetworkEffects } from './effects/network.effects';
import { EditCreateNetworkComponent } from './containers/edit-create-network/edit-create-network.component';
import { NetworkService } from './services';
import { NetworkComponent } from './components/network/network.component';

@NgModule({
  declarations: [NetworksPageComponent, EditCreateNetworkComponent, NetworkComponent],
  entryComponents: [EditCreateNetworkComponent],
  providers: [NetworkService],
  imports: [
    CommonModule,
    NetworkRoutingModule,
    SharedModule,
    StoreModule.forFeature('network', fromNetwork.reducer),
    EffectsModule.forFeature([NetworkEffects])
  ]
})
export class NetworkModule { }
