import { Component, OnInit, Inject } from '@angular/core';
import { DeviceTypes } from '../../../../../../shared/enums';
import { FormBuilder, Validators } from '@angular/forms';
import * as fromNetwork from '../../reducers/network.reducer';
import { Store } from '@ngrx/store';
import { CreateNetwork, UpdateNetwork } from '../../actions/network.actions';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { INetwork } from '../../../../../../shared/interfaces/network.interface';
import { Actions, ofType } from '@ngrx/effects';
import { NetworkApiActionTypes } from '../../actions/network-api.actions';

@Component({
  selector: 'app-edit-create-network',
  templateUrl: './edit-create-network.component.html',
  styleUrls: ['./edit-create-network.component.css'],
})
export class EditCreateNetworkComponent implements OnInit {

  deviceTypes = DeviceTypes;

  isEditMode = false;


  networkForm = this.fb.group({
    name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(10)]],
    deviceType: [this.deviceTypes.Dlink, [Validators.required]],
    lastSuccessfulConn: [this._getCurrentTimeStamp(), [Validators.required, Validators.minLength(7), Validators.maxLength(20)]]
  });

  constructor(
    private fb: FormBuilder,
    private store: Store<fromNetwork.State>,
    public dialogRef: MatDialogRef<EditCreateNetworkComponent>,
    @Inject(MAT_DIALOG_DATA) public network: INetwork,
    private actions$: Actions
  ) {
    this.actions$.pipe(
        ofType(NetworkApiActionTypes.CreateNetworkSuccess, NetworkApiActionTypes.UpdateNetworkSuccess)
      ).subscribe(() => this.dialogRef.close());
  }

  private _getCurrentTimeStamp(): number {
    return new Date().getTime() / 1000;
  }

  onSubmit() {
    if (this.isEditMode) {
      this.store.dispatch(new UpdateNetwork({ id: this.network.id, changes: this.networkForm.value }));
    } else {
      this.store.dispatch(new CreateNetwork(this.networkForm.value));
    }
  }

  ngOnInit() {
    if (this.network) {
      this.isEditMode = true;
      this.networkForm.patchValue(this.network);
    }
  }

}
