import { Injectable } from "@angular/core";
import { MatSnackBar } from '@angular/material/snack-bar';



@Injectable()
export class ToastService {
    constructor(private snackbar: MatSnackBar) {}

    showToast(message: string) {
        this.snackbar.open(message, '', {duration: 3000});
    }
}