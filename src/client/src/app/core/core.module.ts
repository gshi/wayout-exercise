import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CookieModule } from 'ngx-cookie';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingBarModule } from '@ngx-loading-bar/core';


import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { ApiService, ToastService } from './services';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    LoadingBarModule,
    RouterModule,
    SharedModule
  ],
  declarations: [HeaderComponent],
  providers: [
    ApiService,
    ToastService
  ],
  exports: [
    HeaderComponent,
    LoadingBarModule
  ]
})
export class CoreModule { }
