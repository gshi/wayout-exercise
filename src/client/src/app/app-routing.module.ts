import { NgModule } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Route[] = [
    {
      path: '',
      pathMatch: 'full',
      component: HomeComponent
    },

    {
        path: 'networks',
        loadChildren: './network/network.module#NetworkModule'
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
