

export enum DeviceTypes {
    Dlink = 'D-Link',
    TPlink = 'TP-link',
    Netgear = 'Netgear'
}