

export interface INetwork {
    _id?: string;
    id: string;
    name: string;
    timeCreated?: number;
    deviceType: string;
    lastSuccessfulConn?: number;
}