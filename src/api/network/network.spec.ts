import { NetworkController } from './network.controller';
import { fake } from 'sinon';
import { Model } from 'mongoose';
import { expect } from 'chai';
import { INetwork } from '../../shared/interfaces';
import { DeviceTypes } from '../../shared/enums';

class MockNetworkModel extends Model {
    static find = fake();
    static create = fake();
    static update = fake();
    static deleteOne = fake();
}

const networkController = new NetworkController(MockNetworkModel);

describe('network controller tests', () => {
    it('should get all networks form db', done => {
        networkController.get();
        expect(MockNetworkModel.find.callCount).to.equal(1);
        done();
    });

    it('should try to create a network', done => {
        const network = {
            name: 'test',
            deviceType: DeviceTypes.Dlink,
            lastSuccessfulConn: 999999999999
        } as INetwork;
        networkController.create(network);

        expect(MockNetworkModel.create.calledWith(network)).to.equal(true);
        expect(MockNetworkModel.create.callCount).to.equal(1);

        done();
    });

    it('should tye to update a network', done => {
        const network = {
            name: 'test',
            deviceType: DeviceTypes.Dlink,
            lastSuccessfulConn: 999999999999
        } as INetwork;

        const networkId = 'yreuwihkfdsaieuryrew';
        networkController.update(networkId, network);

        expect(MockNetworkModel.update.calledWith({ id: networkId }, network)).to.equal(true);
        expect(MockNetworkModel.update.callCount).to.equal(1);

        done();
    });

    it('should try to delete a network', done => {
        const networkId = 'yreuwihkfdsaieuryrew';

        networkController.delete(networkId);

        expect(MockNetworkModel.deleteOne.calledWith({ id: networkId })).to.equal(true);
        expect(MockNetworkModel.deleteOne.callCount).to.equal(1);

        done();
    });


});