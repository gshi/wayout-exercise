import { Router } from 'express';
import { NetworkController } from './network.controller';
import { INetwork } from '../../shared/interfaces';
import { logger } from '../../logger';
import { NetworkModel } from '../../db/models';


const networkController = new NetworkController(NetworkModel);

const router = Router();


router.get('/', (req, res, next) => {
    logger.info('getting networks', { actionId: 'networkGet' });
    networkController.get()
        .then(val => {
            logger.info('get network success', { actionId: 'networkGet' });
            res.send(val);
        })
        .catch(err => next(err));
});

router.post('/', (req, res, next) => {
    logger.info('adding new network', { actionId: 'networkPost' });
    const network = req.body as INetwork;
    networkController.create(network)
        .then(val => {
            logger.info(`adding new network success networkId=${val.id}`, { actionId: 'networkPost' });
            res.send(val);
        })
        .catch(err => next(err));
});

router.put('/:networkId', (req, res, next) => {
    const networkId = req.params.networkId;
    const network = req.body as INetwork;
    logger.info(`updating network id=${networkId}`, { actionId: 'networkPut' });
    networkController.update(networkId, network)
        .then(val => {
            logger.info(`updating network success id=${networkId}`, { actionId: 'networkPut' });
            res.send(val);
        })
        .catch(err => next(err));
});

router.delete('/:networkId', (req, res, next) => {
    const networkId = req.params.networkId;
    logger.info(`deleting network id=${networkId}`, { actionId: 'networkDelete' });
    networkController.delete(networkId)
        .then(() => {
            logger.info(`deleting network success id=${networkId}`, { actionId: 'networkDelete' });
            res.send();
        })
        .catch(err => next(err));
});


export default router;