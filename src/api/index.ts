import Router from 'express';
import networkRouter from './network/network.router';

const router = Router();

router.get('/check', (req, res, next) => {
    res.send('hello there');
});


router.use('/network', networkRouter);

export default router;
