# Fullstack exercise

This project is a fork of a template I have made for my personal projects.
* BE in node.js with typescript + shared intefaces and enums with client
* FE in Angular 7 + Material and NGRX
* DB - Mongo db hosted on Azure

demo is running on http://wayout.aravatickets.com

enjoy!

## Installation



```bash
cd project_directory
npm i
```
will install both the client and the server dipendencies.

## Usage
to run the server:
```bash
npm run start:backend
```
to run the angular client:
```bash
npm run start:client
```
to get a compiled build of both the client and the backend:
```bash
npm run build
```
to run the project build:
```bash
npm run 
```


## Testing
```bash
npm test
```

Only api tests for now:(